#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main()
{
    int pipe1[2], pipe2[2];
    pid_t pid;

    // Create pipes for communication between parent and child
    if (pipe(pipe1) == -1 || pipe(pipe2) == -1) {
        perror("Pipe error");
        return 1;
    }

    // Create child process
    pid = fork();
    if (pid < 0) {
        perror("Fork error");
        return 1;
    }
    if (pid == 0) {
        // Child process

        // Close write end of first pipe
        close(pipe1[1]);

        // Read string from parent through first pipe
        char input_str[100];
        read(pipe1[0], input_str, 100);

        // Modify the string to uppercase
        int i;
        for (i = 0; i < strlen(input_str); i++) {
            input_str[i] = toupper(input_str[i]);
        }

        // Close read end of second pipe
        close(pipe2[0]);

        // Write the modified string back to parent through second pipe
        write(pipe2[1], input_str, strlen(input_str));
    } else {
        // Parent process

        // Close read end of first pipe
        close(pipe1[0]);

        // Write string to child through first pipe
        char str[] = "Hello";
        write(pipe1[1], str, strlen(str));

        // Close write end of second pipe
        close(pipe2[1]);

        // Read the modified string from child through second pipe
        char output_str[100];
        read(pipe2[0], output_str, 100);

        // Wait for child process to finish
        wait(NULL);

        // Display the modified string
        printf("Child: %s\nParent: %s\n", str, output_str);
    }

    return 0;
}